Задание:
1) Открыть http://way2automation.com/way2auto_jquery/menu.php (можно авторизоваться без куки)
2) Открыть menu with submenu
3) Навестись на элемент с подменю
4) Убедиться, что подменю видимо
    
Запуск теста:

1) В файле \src\test\resources\config.properties заполнить проперти соответствующими значениями:  
way2.user.userName - логин пользователя
way2.user.password - пароль пользователя 	 

1) mvn clean -Dtest=ActionsCase test