package cases;

import io.github.bonigarcia.wdm.FirefoxDriverManager;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.core.io.ClassPathResource;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import pages.ActionsPage;
import pages.LoginPage;

import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import static org.testng.Assert.fail;


public class ActionsCase {

    private final String LOGIN_PAGE_URL = "http://way2automation.com/way2auto_jquery/index.php";
    private final String MENU_PAGE_URL = "http://way2automation.com/way2auto_jquery/menu.php";
    private String userName = "";
    private String userPassword = "";
    private WebDriver driver;
    private ActionsPage actionsPage;
    private LoginPage loginPage;

    @BeforeClass
    public void setUp() throws Exception {
        FirefoxDriverManager.getInstance().version("0.19.1").setup();
        driver = new FirefoxDriver();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        driver.manage().timeouts().pageLoadTimeout(20, TimeUnit.SECONDS);
        driver.manage().timeouts().setScriptTimeout(20, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        initProperties();
    }

    @Test
    public void testActions() throws IOException {
        logIn();
        chooseMenuItem();
        Assert.assertTrue(isSubMenuItemVisible());
    }

    private boolean isSubMenuItemVisible() {
        return new WebDriverWait(driver, 20).until(ExpectedConditions.elementToBeClickable(actionsPage.getSubMenuItem())).isDisplayed();
    }

    private void chooseMenuItem() {
        driver.get(MENU_PAGE_URL);
        actionsPage = new ActionsPage(driver);
        actionsPage.clickElement(actionsPage.getMainMenuWithSubMenu());
        driver.switchTo().frame(actionsPage.getFrameSubMenu());
        Actions builder = new Actions(driver);
        builder.click(actionsPage.getMenuItemElement())
                .sendKeys(Keys.ARROW_RIGHT)
                .build().perform();
    }

    private void logIn() throws IOException {
        driver.get(LOGIN_PAGE_URL);
        loginPage = new LoginPage(driver);
        loginPage.signInBtnClick();
        loginPage.enterUserName(userName);
        loginPage.enterPassword(userPassword);
        loginPage.submitClick();
    }

    private void initProperties() throws IOException {
        Properties config = new Properties();
        try {
            config.load(new ClassPathResource("config.properties").getInputStream());
            userName = config.getProperty("way2.user.userName");
            userPassword = config.getProperty("way2.user.password");
        } catch (IOException ex) {
            ex.printStackTrace();
            fail("File 'config.properties' can't be open!");
        }
    }

    @AfterClass
    public void tearDown() throws Exception {
        driver.quit();
    }
}
