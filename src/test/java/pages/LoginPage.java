package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LoginPage extends BasePage {

    @FindBy(xpath = "//a[contains(text(),'Signin')]")
    private WebElement signInBtn;

    @FindBy(xpath = "//div[@id='login']/form[@id='load_form']/fieldset[1]/input")
    private WebElement userNameElement;

    @FindBy(xpath = "//div[@id='login']/form[@id='load_form']/fieldset[2]/input")
    private WebElement userPasswordElement;

    @FindBy(xpath = "//div[@id='login']/form[@id='load_form']/div[@class='bottom row']/div[@class='span_1_of_4']/input[@class='button']")
    private WebElement submitBtnElement;

    public LoginPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public void signInBtnClick() {
        clickElement(signInBtn);
    }

    public void submitClick() {
        clickElement(submitBtnElement);
    }

    public void enterUserName(String userName) {
        sendKeysToElement(userNameElement, userName);
    }

    public void enterPassword(String userPassword) {
        sendKeysToElement(userPasswordElement, userPassword);
    }

}
