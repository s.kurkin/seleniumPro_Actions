package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;


public class ActionsPage extends BasePage {

    //Main menu
    @FindBy(xpath = "//li/a[contains(text(),'Menu With Sub Menu')]")
    private WebElement mainMenuEl;

    //Menu item 'Adamsville'
    @FindBy(xpath = "//li[contains(text(),'Adamsville')]")
    private WebElement menuItemEl;

    //Frame
    @FindBy(xpath = ".//div[@id='example-1-tab-2']//iframe")
    private WebElement frameSubMenuEl;

    //SubMenu item 'Sub Menu 1'
    @FindBy(xpath = ".//*[@id='ui-id-3']")
    private WebElement menuSubItemEl;

    public ActionsPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public WebElement getMainMenuWithSubMenu() {
        return getElement(mainMenuEl);
    }

    public WebElement getMenuItemElement() {
        return getElement(menuItemEl);
    }

    public WebElement getFrameSubMenu() {
        return getElement(frameSubMenuEl);
    }

    public WebElement getSubMenuItem() {
        return getElement(menuSubItemEl);
    }
}
